if (live_enabled && live_call()) return live_result;


if (keyboard_check_pressed(ord("1"))) {
	with(new Panel("Options", 30, 30, 250, 180)) {
		back_color = c_red;
		corner_radius = 20;
		
		with (addGrid(new Grid("OptionsGrid", 1, [1,2,1]))) {
			padding = 20;
			scrollable = true;			
			with (addGrid(new Grid("OptionsGridList", 5, 1), 0, 1)) {			
				padding = 40
				panel_surfaces_positioning[0][0] = POSITIONING.MIDDLE_CENTER;
				panel_surfaces_positioning[1][0] = POSITIONING.MIDDLE_CENTER;
				panel_surfaces_positioning[2][0] = POSITIONING.MIDDLE_CENTER;
				panel_surfaces_positioning[3][0] = POSITIONING.MIDDLE_CENTER;
				panel_surfaces_positioning[4][0] = POSITIONING.MIDDLE_CENTER;
				padding = 15;
				border_color = c_fuchsia;
				user_mouse_right_click = function() {
					visible = !visible;
				}
				addWidget(new Label("StartLabel", 50, 5, "[fa_center][fa_middle]Start"), 0, 0);
				addWidget(new Label("MenuLabel", 50, 150, "Menu"), 1, 0);
				addWidget(new Label("OptionsLabel", 50, 5, "Options"), 2, 0);
				addWidget(new Label("CreditsLabel", 50, 5, "Credits"), 3, 0);
				addWidget(new Label("QuitLabel", 50, 5, "Quit"), 4, 0);			
			}
		
			with (addWidget(new Label("OptionsTitleLabel", 10, 10, "[c_lime][wave][rainbow][fnt_Demo]Options[/rainbow][/wave]"), 0, 0)) {
				user_mouse_left_click = function() {
					with (new Panel("Dialog", 100, 100, 300, 200)) {
						with (addGrid(new Grid("Default", [90,10], 1))) {
							addWidget(new Label("DialogLabel", 10, 10, "This is a dialog.\nPress OK to close (destroy)."), 0, 0);
							with (addWidget(new Label("OKButtonLabel", 0, 0, "[c_red]OK"), 1, 0)) {
								user_mouse_left_click = function() {
									ui_get_panel_id("Dialog").destroy();
								}
							}
						}
					}				
				}
			}
			with (addWidget(new Label("GoodBadLabel", 10, 40, "You are good [spr_Smiley]"), 0, 0)) {
				user_mouse_wheel_increment = function() {
					text = "You are [c_red]bad";
				}
				user_mouse_wheel_decrement = function() {
					text = "You are good [spr_Smiley]";
				}
				user_mouse_left_click = function() {
					visible = !visible;
					ui_print("I'm now "+string(visible)+" with width "+string(width));
				}
			}
			addWidget(new Label("ClearLabel", 10, 150, "[fnt_Demo][scale,0.5]Enable clear"), 0, 0);				
		}
	}
	
}


if (keyboard_check_pressed(ord("2"))) {
	with (new Panel("Ice Cream", 350, 100, 300, 300)) {
		back_color = c_green;
		with (addGrid(new Grid("Ice Cream Grid", [1,3], [2,1,2]))) {
			border_color = #ffcc00;
			padding = 40;
			addWidget(new Label("WineLabel", 0, 0, "[c_red]Red red wine!"), 1, 0);
		}
	}	
}

if (keyboard_check_pressed(ord("3"))) {
	/*
	with (new Panel("test", 20, 20, 400, 400)) {
		back_color = c_blue;
		with (addGrid(new Grid("grid", 1, 1))) {
			scrollable = true;
			panel_surfaces_positioning[0][0] = POSITIONING.AUTOMATIC;
			padding = 20;
			addWidget(new Label("HelloWorldLabel", 50, 50, "hello world!"), 0, 0);
			addWidget(new Label("", 50, 150, "[fa_right]whatever"), 0, 0);
			with (addWidget(new Label("HelloWorldLabel", 50, 300, "[fa_left][c_green][scale,2]This is a very, very long text which should wrap at the panel's width and height."), 0, 0)) {
				text_wrap_width = parent_widget_id.width - 2*parent_widget_id.padding;
			}
		}
	}*/
	
	with (new Panel("OptionsPanel", 400, 400, 100, 100)) {
		back_color = #ccffdd;
		with (addGrid(new Grid("OptionsPanelGrid", 2, [1,3]))) {
			scrollable = true;
			panel_surfaces_positioning[0][1] = POSITIONING.AUTOMATIC;
			panel_surfaces_positioning[1][1] = POSITIONING.EXACT;
			padding = 30;
			addWidget(new Label("HiLabel", 50, 10, "Qué onda mano"), 1, 1);
		}
	}
}







if (keyboard_check_pressed(vk_f1)) {
	obj_UI.debug = !obj_UI.debug;
}

if (keyboard_check_pressed(vk_escape)) {
	game_restart();
}
