if (live_enabled && live_call()) return live_result;


/*
with (ui_panel_create("Red", 150, 100, 500, 100)) {
	back_color = c_red;	
	draggable = false;
}

with (ui_panel_create("Blue", 100, 50, 300, 200)) {
	border_width = 10;	
	back_color = c_blue;
}

with(ui_panel_create("Green", 80, 20, 200, 200)) {
	back_color = c_green;
}


with(ui_panel_create("Fuchsia", 400, 400, 50, 50)) {
	back_color = c_fuchsia;
	border_width = 0;
	resizable = false;
}

with(ui_panel_create("Kuchi", 500, 500, 140, 90)) {
	back_color = #ab46df;
}

with(ui_panel_create("Orange", 300, 300, 30, 80)) {
	back_color = c_orange;
	min_height = 60;
}

with(ui_panel_create("Gold", 180, 90, 100, 80)) {
	back_color = #ffcc00;
	user_mouse_right_click = function() {
		show_message("Hei");
		ui_panel_destroy("Gold");
	}
	min_width = 40;
}

with(ui_panel_create("Dark Gray", 90, 90, 120, 60)) {
	back_color = #666666;
	draggable = false;
	resizable = false;
}

with(ui_grid_create("Test", "Red", 3, 2)) {
}

with(ui_grid_create("Test2", "Kuchi", 1, [20,60,20])) {
	padding = 10;
}
*/


/*
with(ui_panel_create("Random", 200, 200, 200, 200, 1, [1,2,1])) {
	back_color = make_color_rgb(irandom(255), irandom(255), irandom(255));
	with (self.ui_grids[self.current_grid]) {		
		padding = 5;
	}
}

ui_label_create("Label", "Page1", "Random", 10, 10, "[c_lime][wave]Options[/wave]");
ui_label_create("New", "Page1", "Random", 10, 20, "You are good [spr_Smiley]");
ui_label_create("New", "Page1", "Random", 10, 40, "[fnt_Demo][scale,0.5]Enable alpha");
ui_label_create("New", "Page1", "Random", 10, 60, "[fnt_Demo][scale,0.5]Enable clear");
ui_label_create("New", "Page1", "Random", 10, 80, "[fnt_Demo][scale,0.5]Enable sound");
ui_label_create("New", "Page1", "Random", 10, 100, "[fnt_Demo][scale,0.5]Enable music");
ui_label_create("New", "Page1", "Random", 10, 120, "[fnt_Demo][scale,0.5]Enable antialiasing");
ui_label_create("New", "Page1", "Random", 10, 140, "[fnt_Demo][scale,0.5]Enable the best options ever");
ui_label_create("New", "Page1", "Random", 10, 160, "[fnt_Demo][scale,0.5]Enable fullscreen");

ui_grid_create_grid("Hi", "Page1", "Random", 0, 2, 5, 1);
*/





