if (live_enabled && live_call()) return live_result;

ui_backup_draw_params();

/*===========================================================================
	Handle panel
	1) Draw (each panel will call its own draw event on its children
	2) Manage functions and callbacks on mouse events
===========================================================================*/

#region Panel draw

	var _n = array_length(ui_panels);

	// Sort order by depth: array is sorted by max depth first, so first position is drawn first, second on top of that, etc.
	// i.e. objects further to the right of the array are drawn on top of the objects to the left (i.e. they have smaller depth number)
	// Newer objects with same depth are drawn on top of older objects
		
	for (var _i=0; _i<_n; _i++) {
		var _id = ui_panels[_i];
		_id.draw();
	}

	
#endregion

#region Event handling

	
	// Sort order by depth: array is sorted by min depth first, so first position is handled first, second on top of that, etc.
	// i.e. objects further to the right of the array are handled on top of the objects to the left (i.e. they have smaller depth number)
	// Newer objects with same depth are handled UNDER older objects
		
	// Only callbacks from widgets with enabled=true are processed and only one callback per event is fired (according to sort above)
	
	function ui_determine_focus_mouseover(_idx_grid, _id_parent) {		
		if (_idx_grid.grid_cells_mouseover_row >= 0 && _idx_grid.grid_cells_mouseover_col >= 0) {
			if (is_array(_idx_grid.grid_cells[_idx_grid.grid_cells_mouseover_row][_idx_grid.grid_cells_mouseover_col])) { // Widgets, determine which one
				var _n = array_length(_idx_grid.grid_cells[_idx_grid.grid_cells_mouseover_row][_idx_grid.grid_cells_mouseover_col]);
				if (_n == 0) {
					//ui_print("Grid calling mouseover, no widgets present");
					return _idx_grid;					
				}
				else {
					var _i=0; 
					var _found = false;
					while (_i<_n && !_found) {
						if (_idx_grid.grid_cells[_idx_grid.grid_cells_mouseover_row][_idx_grid.grid_cells_mouseover_col][_i].__mouse_over) {							
							_found = true;
							//ui_print("Specific widget calling mouseover");
						}
						else {
							_i++;
						}
					}
					if (!_found) {
						//ui_print("Grid calling mouseover, no widgets being mouseovered");						
						return _idx_grid;
					}
					else {
						// Widget
						return _idx_grid.grid_cells[_idx_grid.grid_cells_mouseover_row][_idx_grid.grid_cells_mouseover_col][_i];
					}
				}
			}
			else { // Child grid, recurse				
				return ui_determine_focus_mouseover(_idx_grid.grid_cells[_idx_grid.grid_cells_mouseover_row][_idx_grid.grid_cells_mouseover_col], _idx_grid);
			}
		}
		else { // Mouse event is happening outside the grid but inside the panel
			//ui_print("Mouse event happening outside grid");			
			return _id_parent;
		}
	}
	
	// Handle mouse enter/click/over/wheel events (all of them apply to the same widget)
	var _i=_n-1;
	var _found = false;
	while (_i>=0 && !_found) {
		var _id = ui_panels[_i];
		if (_id.__mouse_over) {
			if (_id.enabled && _id.visible) {
				var _actual_id = (_id.current_grid >= 0) ? ui_determine_focus_mouseover(_id.ui_grids[_id.current_grid], _id) : _id;
				// Mouse over
				_actual_id.mouse_over();
				if (target_id_mouse_exit != -1 && target_id_mouse_exit != _actual_id && !target_id_mouse_exit.__mouse_over) { // Mouse exit
					 target_id_mouse_exit.mouse_exit();
				}
				target_id_mouse_exit = _actual_id;
				//ui_print("On focus component: "+_actual_id.name+" ("+ui_type_name(_actual_id.type));
				// Mouse enter
				if (!_actual_id.__mouse_over_prev) _actual_id.mouse_enter();
				//if (_actual_id.name == "grid") show_debug_message(string(_actual_id.__mouse_over_prev)+" "+string(_actual_id.__mouse_over));
				// Mouse left click
				if (mouse_left_click) _actual_id.mouse_left_click();
				// Mouse right click
				if (mouse_right_click) _actual_id.mouse_right_click();
				// Mouse middle click
				if (mouse_middle_click) _actual_id.mouse_middle_click();				
				
				// Mouse wheel increment
				if (mouse_wheel_increment) _actual_id.mouse_wheel_increment();
				// Mouse wheel decrement
				if (mouse_wheel_decrement) _actual_id.mouse_wheel_decrement();
				_found = true;
			}
		}
		if (!_found) _i--;
	}
	if (!_found && target_id_mouse_exit != -1) { // Exit the panel
		target_id_mouse_exit.mouse_exit();
		target_id_mouse_exit = -1;
	}

#endregion


#region Hold and release function/callback handling

	if (obj_UI.ui_active != noone) {
		// Mouse left hold
		if (mouse_left_hold) ui_panels[obj_UI.ui_active].mouse_left_hold();
		// Mouse right hold
		if (mouse_right_hold) ui_panels[obj_UI.ui_active].mouse_right_hold();
		// Mouse middle hold
		if (mouse_middle_hold) ui_panels[obj_UI.ui_active].mouse_middle_hold();
		// Mouse left released
		if (mouse_left_released) {
			ui_panels[obj_UI.ui_active].mouse_left_release();
			obj_UI.ui_active = noone;			
		}
		// Mouse right released
		if (mouse_right_released) ui_panels[obj_UI.ui_active].mouse_right_release();
		// Mouse middle released
		if (mouse_middle_released) ui_panels[obj_UI.ui_active].mouse_middle_release();	
	}

#endregion

#region Debug HUD for UI

	draw_set_valign(fa_bottom);
	draw_set_halign(fa_left);
	draw_text(gui_mouse_x, gui_mouse_y, debug_text_mouse);

	var _id = obj_UI.ui_active;
	draw_text(10, 30, "UI ACTIVE: "+string(_id)+(_id != noone ? " - "+ui_panels[_id].name : ""));
	
	draw_set_halign(fa_right);
	draw_text(gui_width-10, 30, "FPS: "+string(fps_real));

#endregion

ui_restore_draw_params();
