if (live_enabled && live_call()) return live_result;

// General Variables and macros
ui_map = ds_map_create();
ui_panel_array = [];
ui_active = noone;
debug = true;
debug_text_mouse = "";
mark_for_deletion = [];


// Components

#region Mouse

	mouse_left_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_left);
	mouse_middle_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_middle);
	mouse_right_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_right);
	
	mouse_left_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_left);
	mouse_middle_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_middle);
	mouse_right_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_right);

	mouse_left_hold_array = array_create(UI_HOLD_ARRAY_SIZE+1, false);
	mouse_middle_hold_array = array_create(UI_HOLD_ARRAY_SIZE+1, false);
	mouse_right_hold_array = array_create(UI_HOLD_ARRAY_SIZE+1, false);
	
	mouse_left_hold = false;
	mouse_middle_hold = false;
	mouse_right_hold = false;
	
	array_push(mouse_left_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_left));
	ui_array_resize_from_end(mouse_left_hold_array, UI_HOLD_ARRAY_SIZE+1);
	array_push(mouse_middle_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_middle));
	ui_array_resize_from_end(mouse_middle_hold_array, UI_HOLD_ARRAY_SIZE+1);
	array_push(mouse_right_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_right));
	ui_array_resize_from_end(mouse_right_hold_array, UI_HOLD_ARRAY_SIZE+1);
	
	mouse_wheel_increment = mouse_wheel_up();
	mouse_wheel_decrement = mouse_wheel_down();
	
	gui_mouse_x = device_mouse_x_to_gui(DEVICE_MOUSE);
	gui_mouse_y = device_mouse_y_to_gui(DEVICE_MOUSE);
	gui_mouse_x_prev = gui_mouse_x;
	gui_mouse_y_prev = gui_mouse_y;
		
	target_id_mouse_exit = -1;
	
#endregion

#region Screen

	gui_width = display_get_gui_width();
	gui_height = display_get_gui_height();
	
#endregion


ui_print(LIB+" "+VERSION+" by José Alberto Bonilla Vera");
if (!variable_global_exists("__scribble_lcg")) {
	ui_print("Note: Scribble not found, using native draw text functions.");
}
