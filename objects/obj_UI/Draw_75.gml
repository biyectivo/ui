if (live_enabled && live_call()) return live_result;

// Destroy all panels marked for deletion
var _n = array_length(self.mark_for_deletion);
for (var _i=0; _i<_n; _i++) {
	// @TODO Destroy all children
	// Destroy reference
	var _j=0;
	var _m = array_length(ui_panels);
	var _found = false;
	while (_j<_m && !_found) {
		if (ui_panels[_j] == mark_for_deletion[_i]) {
			_found = true;
		}
		else {
			_j++;
		}
	}
	if (_found) {
		array_delete(ui_panels, _j, 1);
		obj_UI.ui_active = noone;
	}
}
self.mark_for_deletion = [];
