// Update mouse

#region Mouse

	mouse_left_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_left);
	mouse_middle_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_middle);
	mouse_right_click = device_mouse_check_button_pressed(DEVICE_MOUSE, mb_right);
	
	mouse_left_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_left);
	mouse_middle_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_middle);
	mouse_right_released = device_mouse_check_button_released(DEVICE_MOUSE, mb_right);

	array_push(mouse_left_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_left));
	ui_array_resize_from_end(mouse_left_hold_array, UI_HOLD_ARRAY_SIZE+1);		
	array_push(mouse_middle_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_middle));
	ui_array_resize_from_end(mouse_middle_hold_array, UI_HOLD_ARRAY_SIZE+1);
	array_push(mouse_right_hold_array, device_mouse_check_button(DEVICE_MOUSE, mb_right));
	ui_array_resize_from_end(mouse_right_hold_array, UI_HOLD_ARRAY_SIZE+1);

	mouse_left_hold = ui_array_count(mouse_left_hold_array, true) >= UI_HOLD_ARRAY_SIZE;
	mouse_middle_hold = ui_array_count(mouse_middle_hold_array, true) >= UI_HOLD_ARRAY_SIZE;
	mouse_right_hold = ui_array_count(mouse_right_hold_array, true) >= UI_HOLD_ARRAY_SIZE;
	
	mouse_wheel_increment = mouse_wheel_up();
	mouse_wheel_decrement = mouse_wheel_down();
	
	gui_mouse_x_prev = gui_mouse_x;
	gui_mouse_y_prev = gui_mouse_y;
	gui_mouse_x = device_mouse_x_to_gui(DEVICE_MOUSE);
	gui_mouse_y = device_mouse_y_to_gui(DEVICE_MOUSE);	
	
	
#endregion

#region Screen

	gui_width = display_get_gui_width();
	gui_height = display_get_gui_height();
	
#endregion

#region Widget updates

	// Update widgets
	var _n = array_length(ui_panels);
	for (var _i=0; _i<_n; _i++) {
		var _id = ui_panels[_i];
		_id.__update();
	
		if (_id.current_grid >= 0) {
			_id.ui_grids[_id.current_grid].__update();
		}
	}

#endregion
