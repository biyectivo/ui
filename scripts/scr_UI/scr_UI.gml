#region General macros

	#macro LIB "UI"
	#macro VERSION "0.1"
	#macro ui obj_UI.ui_map
	#macro ui_panels obj_UI.ui_panel_array

#endregion

#region Configurable macros

	#macro RESIZE_CORNER_SIZE		20
	#macro WIDGET_MIN_WIDTH			10
	#macro WIDGET_MIN_HEIGHT		10
	#macro UI_HOLD_ARRAY_SIZE		6
	
	#macro VERTICAL_SCROLL_SPEED	15
	
	#macro DRAG_CURSOR				cr_drag
	#macro RESIZE_CURSOR			cr_size_nwse
	#macro DEVICE_MOUSE				0
	
#endregion

#region Enums

	enum SIZING {
		ABSOLUTE,		// px 
		RELATIVE		// % of parent
	}
	
	enum UI_TYPE {
		PANEL,
		GRID,
		LABEL
	}

	enum EVENTS {
		MOUSE_OVER,
		MOUSE_ENTER,
		MOUSE_EXIT,
		LEFT_CLICK,
		RIGHT_CLICK,
		MIDDLE_CLICK,
		LEFT_HOLD,
		RIGHT_HOLD,
		MIDDLE_HOLD,
		LEFT_RELEASE,
		RIGHT_RELEASE,
		MIDDLE_RELEASE,
		WHEEL_INCREMENT,
		WHEEL_DECREMENT
	}
	
	enum POSITIONING {		
		EXACT,
		AUTOMATIC,
		TOP_LEFT,
		TOP_CENTER,
		TOP_RIGHT,
		MIDDLE_LEFT,
		MIDDLE_CENTER,
		MIDDLE_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_CENTER,
		BOTTOM_RIGHT		
	}

#endregion

#region Helper functions
	
	
	function text_get_bbox(_x, _y, _w, _h) {		
		if (draw_get_halign() == fa_left && draw_get_valign() == fa_top)		return {left: _x,		top: _y,		right: _x+_w,		bottom: _y+_h};
		if (draw_get_halign() == fa_left && draw_get_valign() == fa_middle)		return {left: _x,		top: _y-_h/2,	right: _x+_w,		bottom: _y+_h/2};
		if (draw_get_halign() == fa_left && draw_get_valign() == fa_bottom)		return {left: _x,		top: _y-_h,		right: _x+_w,		bottom: _y};
		if (draw_get_halign() == fa_center && draw_get_valign() == fa_top)		return {left: _x-_w/2,	top: _y,		right: _x+_w/2,		bottom: _y+_h};
		if (draw_get_halign() == fa_center && draw_get_valign() == fa_middle)	return {left: _x-_w/2,	top: _y-_h/2,	right: _x+_w/2,		bottom: _y+_h/2};
		if (draw_get_halign() == fa_center && draw_get_valign() == fa_bottom)	return {left: _x-_w/2,	top: _y-_h,		right: _x+_w/2,		bottom: _y};
		if (draw_get_halign() == fa_right && draw_get_valign() == fa_top)		return {left: _x-_w,	top: _y,		right: _x,			bottom: _y+_h};
		if (draw_get_halign() == fa_right && draw_get_valign() == fa_middle)	return {left: _x-_w,	top: _y-_h/2,	right: _x,			bottom: _y+_h/2};
		if (draw_get_halign() == fa_right && draw_get_valign() == fa_bottom)	return {left: _x-_w,	top: _y-_h,		right: _x,			bottom: _y};		
	}
	
	function draw_rectangle_width_color(_x1, _y1, _x2, _y2, _width, _color, _outline) {
		if (!_outline) draw_rectangle_color(_x1, _y1, _x2, _y2, _color, _color, _color, _color, false);
		draw_line_width_color(_x1, _y1, _x2, _y1, _width, _color, _color);
		draw_line_width_color(_x1, _y1, _x1, _y2, _width, _color, _color);
		draw_line_width_color(_x1, _y2, _x2, _y2, _width, _color, _color);
		draw_line_width_color(_x2, _y1, _x2, _y2, _width, _color, _color);
	}
	
	function ui_add_to_map(_name, _type, _id) {
		if (_name == "" || ds_map_exists(ui, _name)) {			
			var _i=1;
			var _new_name = ui_type_name(_type)+string(_i);
			while (ds_map_exists(ui, _new_name)) {
				_i++;
				_new_name = ui_type_name(_type)+string(_i);
			}
			_id.name = _new_name;
			var _stack = debug_get_callstack(1);
			var _rest_of_text = _name == "" ? "without an identifier" : "with requested identifier '"+_name+"', but the identifier already exists";
			ui_print("WARNING: At "+_stack[0]+" > "+ui_type_name(_type)+" created "+_rest_of_text+". The "+ui_type_name(_type)+" was created with identifier '"+_new_name+"' instead.");
			ds_map_add(ui, _new_name, self);
		}
		else {
			ds_map_add(ui, _name, self);
		}		
	}
	
	function ui_print(_str) {
		var _name = (!is_struct(self) || !variable_struct_exists(self, "type") || self.type == noone) ? "" : self.name;
		var _type = (!is_struct(self) || !variable_struct_exists(self, "type") || self.type == noone) ? "" : ui_type_name(self.type);
		var _idstr = (!is_struct(self) || !variable_struct_exists(self, "type") || self.type == noone) ? "" : "(ID "+_name+" / "+_type+") ";		
		var _logstr = "["+LIB+" "+string(VERSION)+"] "+_idstr+_str;
		show_debug_message(_logstr);
		if (window_get_fullscreen()) {
			obj_UI.debug_text_mouse = _logstr;
		}
	}
	
	function ui_type_name(_idx) {
		switch (_idx) {
			case UI_TYPE.PANEL:		return "Panel";
			case UI_TYPE.GRID:		return "Grid";
			case UI_TYPE.LABEL:		return "Label";
		}
	}

	function ui_array_find(_array, _element) {
		var _n = array_length(_array);
		var _i = 0; 
		var _found = false;
		while (_i<_n && !_found) {
			_found = (_array[_i] == _element);
			if (!_found) _i++;
		}
		if (_found) {
			return _i;
		}
		else {
			return -1;
		}
	}


	/// @function backup_draw_params
	/// @description Backup the draw parameters to variables

	function ui_backup_draw_params() {
		tmpDrawFont = draw_get_font();
		tmpDrawHAlign = draw_get_halign();
		tmpDrawVAlign = draw_get_valign();
		tmpDrawColor = draw_get_color();
		tmpDrawAlpha = draw_get_alpha();	
	}

	/// @function restore_draw_params
	/// @description Restore the draw parameters from variables

	function ui_restore_draw_params() {
		draw_set_font(tmpDrawFont);
		draw_set_halign(tmpDrawHAlign);
		draw_set_valign(tmpDrawVAlign);
		draw_set_color(tmpDrawColor);
		draw_set_alpha(tmpDrawAlpha);	
	}
	
	///@function				ui_array_to_string(array)
	///@description				prints an array to log
	///@param		{array}		array		the array
	///@return		{string}	the string representation of the array
	function ui_array_to_string(_array) {
		var _n = array_length(_array);
		var _str = "[";
		for (var _i=0; _i<_n; _i++) {
			if (is_array(_array[_i])) {
				_str += ui_array_to_string(_array[_i]);
			}
			else {
				_str += string(_array[_i]);
			}
			if (_i<_n-1) _str += ",";
		}
		_str += "]";
		return _str;
	}
	
	
	///@function				ui_array_resize_from_end(array, size)
	///@description				resizes an array from the end
	///@param		{array}		array		the array
	///@param		{int}		size		the new size to resize to
	function ui_array_resize_from_end(_array, _new_size) {
		var _n = array_length(_array);
		if (_new_size < _n) {		
			for (var _i=0; _i<_new_size; _i++) {
				_array[@_i] = _array[_i+_n-_new_size];
			}
			array_resize(_array, _new_size);
		}
	}
	
	///@function				ui_array_count(array, value)
	///@description				counts the number of occurrences of a value within an array
	///@param		{array}		array		the array
	///@param		{*}			val			the value to check against
	///@return					the count of occurrences of val
	function ui_array_count(_array, _val) {
		var _n = array_length(_array);
		var _cnt = 0;
		for (var _i=0; _i<_n; _i++) {
			if (_array[_i] == _val) {
				_cnt++;
			}
		}
		return _cnt;
	}
	
	///@function				ui_array_normalize(array)
	///@description				normalizes a numeric array, i.e., converts each value into a value within [0,1] such that the sum of all normalized elements is 1
	///@param		{array}		array		the array	
	function ui_array_normalize(_array) {
		var _tolerance = 0.00000001;
		var _n = array_length(_array);
		var _sum = 0;
		for (var _i=0; _i<_n; _i++) {
			_sum += _array[_i];
		}
		if (abs(_sum)>_tolerance && abs(_sum-1)>_tolerance) { // 0 makes no sense, 1 nothing to do
			for (var _i=0; _i<_n; _i++) {
				_array[@ _i] = _array[_i]/_sum;
			}
		}
	}


#endregion

#region Widget class

	function Widget(_name, _x, _y, _w, _h, _sizing = SIZING.ABSOLUTE) constructor {
		// =========================
		// Dimensions and Positions
		// =========================
		name = _name;
		type = -1;
		x = _x;
		y = _y;
		draw_x = _x;
		draw_y = _y;
		width = _w;
		height = _h;
		min_width = WIDGET_MIN_WIDTH;
		min_height = WIDGET_MIN_HEIGHT;
		enabled = true;
		visible = true;
		sizing = _sizing;
		parent_widget_id = noone;
		
		// =========================
		// PRIVATE
		// =========================
		#region Private
			__x1 = function() {
				x1 = x;
			}
			__y1 = function() {
				y1 = y;
			}
			__x2 = function() {
				x2 = x1+width;
			}
			__y2 = function() {
				y2 = y1+height;
			}
			
						
			__drag_resize_point_x = -1;
			__drag_resize_point_y = -1;
			__dragging = false;
			__resizing = false;
			__mouse_over = false;
			__mouse_over_prev = false;
			__mouse_over_resizable = false;
			
			
			__update = function() {
				__x1();
				__y1();
				__x2();
				__y2();
			
				__mouse_over_prev = __mouse_over;
				if (type == UI_TYPE.PANEL || type == UI_TYPE.GRID) {
					__mouse_over = visible ? point_in_rectangle(obj_UI.gui_mouse_x, obj_UI.gui_mouse_y, x1, y1, x2, y2) : false;
				}
				else {
					__mouse_over = visible ? point_in_rectangle(obj_UI.gui_mouse_x, obj_UI.gui_mouse_y, draw_x, draw_y, draw_x+width, draw_y+height) : false;
				}
				__mouse_over_resizable = visible ? point_in_rectangle(obj_UI.gui_mouse_x, obj_UI.gui_mouse_y, x2-RESIZE_CORNER_SIZE, y2-RESIZE_CORNER_SIZE, x2, y2) : false;

			}
			
			// Call preemptively to avoid HTML5 bugs
			__update();
		
		#endregion
		
		// =========================
		// Methods
		// =========================
		getDepth = function() {
			return ui_array_find(ui_panels, self);
		}
			
		// =========================
		// Events
		// =========================
		#region Events
		
			user_mouse_over = noone;
			user_mouse_enter = noone;
			user_mouse_exit = noone;
			user_mouse_left_click = noone;
			user_mouse_right_click = noone;
			user_mouse_middle_click = noone;
			user_mouse_left_hold = noone;
			user_mouse_left_release = noone;
			user_mouse_right_hold = noone;
			user_mouse_right_release = noone;
			user_mouse_middle_hold = noone;
			user_mouse_middle_release = noone;
			user_mouse_wheel_increment = noone;
			user_mouse_wheel_decrement = noone;
	
			mouse_over = function() {
				if (obj_UI.debug) {}
				// Call user defined callback
				if (is_method(user_mouse_over))				user_mouse_over();
			}
			mouse_enter = function() {
				if (obj_UI.debug) ui_print("Mouse enter");
				// Call user defined callback
				if (is_method(user_mouse_enter))			user_mouse_enter();
			}
			mouse_exit = function() {
				if (obj_UI.debug) ui_print("Mouse exit");	
				// Call user defined callback
				if (is_method(user_mouse_exit))				user_mouse_exit();		
			}
			mouse_left_click = function() {		
				
				if (obj_UI.ui_active == noone) {
					
					var _id = ui_get_parent_panel_id(self);
					
					var _depth = _id.getDepth();
					// Reorder
					var _tmp = ui_panels[_depth];
					var _n = array_length(ui_panels);
					for (var _i=_depth; _i<_n-1; _i++) {
						ui_panels[_i] = ui_panels[_i+1];
					}
					ui_panels[_n-1] = _tmp;
				
					// Focus on panel
					obj_UI.ui_active = _n-1;
					if (obj_UI.debug) ui_print("Setting active widget - original depth="+string(_depth)+" new depth="+string(_n-1));
				}
				
				if (obj_UI.debug) ui_print("Left click");	
				// Call user defined callback
				if (is_method(user_mouse_left_click))		user_mouse_left_click();
			}
			mouse_right_click = function() {
				if (obj_UI.debug) ui_print("Right click");	
				// Call user defined callback
				if (is_method(user_mouse_right_click))		user_mouse_right_click();	
			}
			mouse_middle_click = function() {
				if (obj_UI.debug) ui_print("Middle click");
				// Call user defined callback
				if (is_method(user_mouse_middle_click))		user_mouse_middle_click();	
			}
			mouse_left_hold = function() {
				if (obj_UI.debug) ui_print("Left mouse button hold");	
				// Call user defined callback
				if (is_method(user_mouse_left_hold))		user_mouse_left_hold();	
			}
			mouse_left_release = function() {
				if (obj_UI.debug) ui_print("Left mouse button release");
				// Call user defined callback
				if (is_method(user_mouse_left_release))		user_mouse_left_release();	
			}
			mouse_right_hold = function() {
				if (obj_UI.debug) ui_print("Right mouse button hold");
				// Call user defined callback
				if (is_method(user_mouse_right_hold))		user_mouse_right_hold();
			}
			mouse_right_release = function() {
				if (obj_UI.debug) ui_print("Right mouse button release");
				// Call user defined callback
				if (is_method(user_mouse_right_release))	user_mouse_right_release();	
			}
			mouse_middle_hold = function() {
				if (obj_UI.debug) ui_print("Middle mouse button hold");
				// Call user defined callback
				if (is_method(user_mouse_middle_hold))		user_mouse_middle_hold();
			}
			mouse_middle_release = function() {
				if (obj_UI.debug) ui_print("Middle mouse button release");
				// Call user defined callback
				if (is_method(user_mouse_middle_release))	user_mouse_middle_release();
			}
			mouse_wheel_increment = function() {
				if (type != UI_TYPE.PANEL && type != UI_TYPE.GRID) {
					parent_widget_id.mouse_wheel_increment();
					if (obj_UI.debug) ui_print("Mouse wheel upwards of parent");
				}
				else {
					if (obj_UI.debug) ui_print("Mouse wheel upwards");
				}
				// Call user defined callback
				if (is_method(user_mouse_wheel_increment))	user_mouse_wheel_increment();
			}
			mouse_wheel_decrement = function() {
				if (type != UI_TYPE.PANEL && type != UI_TYPE.GRID) {
					parent_widget_id.mouse_wheel_decrement();
					if (obj_UI.debug) ui_print("Mouse wheel downwards of parent");
				}
				else {
					if (obj_UI.debug) ui_print("Mouse wheel downwards");
				}				
				// Call user defined callback
				if (is_method(user_mouse_wheel_decrement))	user_mouse_wheel_decrement();
			}

		#endregion
		
	}

#endregion

#region Widgets
	
	#region Panel
	
		function Panel(_name, _x, _y, _w, _h, _sizing = SIZING.ABSOLUTE) : Widget(_name, _x, _y, _w, _h, _sizing) constructor {			
			type = UI_TYPE.PANEL;
			// =========================
			// Styling
			// =========================
			sprite_index = noone;
			border_width = 1;
			border_color = c_dkgray;
			corner_radius = 0;
			back_color = c_gray;
			draggable = true;
			resizable = true;
			
			// =========================
			// Child grids
			// =========================	
			ui_grids = [];
			current_grid = -1;
	
			// =========================
			// Methods
			// =========================
			
			#region Add child grid
				
				addGrid = function(_idx_grid) {
					_idx_grid.parent_widget_id = self;
					array_push(ui_grids, _idx_grid);
					current_grid++;
					return _idx_grid;
				}
			
			#endregion
			
			__super_update = __update;
			method(self, __super_update);
			__update = function() {
				__super_update();
				if (__resizing || __dragging) {
					//ui_print(__resizing ? "Resizing" : "Dragging");
				}
				
				// Drag/Resize				
				if (obj_UI.mouse_left_hold && !obj_UI.mouse_left_hold_array[0]) {
					__resizing = __mouse_over_resizable;
					__dragging = __mouse_over;
					__drag_resize_point_x = obj_UI.gui_mouse_x;
					__drag_resize_point_y = obj_UI.gui_mouse_y;
				}
			}
			
			#region Draw
			
				draw = function() {	
					if (self.visible) {
						if (sprite_exists(sprite_index)) {
						}
						else {
							if (corner_radius == 0) {
								draw_rectangle_color(x1, y1, x2, y2, back_color, back_color, back_color, back_color, false);
								for (var _i=0; _i<border_width; _i++) {
									draw_rectangle_color(x1+_i, y1+_i, x2-_i, y2-_i, border_color, border_color, border_color, border_color, true);
								}
							}
							else {
								draw_roundrect_color_ext(x1, y1, x2, y2, corner_radius, corner_radius, back_color, back_color, false);
								for (var _i=0; _i<border_width; _i++) {
									draw_roundrect_color_ext(x1+_i, y1+_i, x2-_i, y2-_i, corner_radius, corner_radius, border_color, border_color, true);
								}
							}
						}
						if (obj_UI.debug) {
							draw_set_halign(fa_center); draw_set_valign(fa_middle); draw_text(x1+(x2-x1)/2, y1+(y2-y1)/2, name+": "+string(self.getDepth()));
							draw_rectangle_color(x2-RESIZE_CORNER_SIZE, y2-RESIZE_CORNER_SIZE, x2, y2, c_white, c_white, c_white, c_white, false);
							draw_circle_color(x, y, 2, c_red, c_red, false);
						}
		
						// Draw current grid
						if (current_grid != -1) {
							ui_grids[current_grid].draw();
						}
					
					}
				}
				
			#endregion
			
			#region Events
				// Inherit mouse over method
				super_mouse_over = mouse_over;
				method(self, super_mouse_over);
				mouse_over = function() {
					super_mouse_over();
					// Resize
					if (self.__mouse_over_resizable && self.resizable) {
						window_set_cursor(RESIZE_CURSOR);
					}
					// Drag
					else if (self.__mouse_over && self.draggable) {
						window_set_cursor(DRAG_CURSOR);
					}				
				}
				
				// Inherit mouse click method
				super_mouse_left_click = mouse_left_click;
				method(self, super_mouse_left_click);
				
				mouse_left_click = function() {
					super_mouse_left_click();
					
				}
				
				// Inherit mouse exit method
				super_mouse_exit = mouse_exit;
				method(self, super_mouse_exit);
				mouse_exit = function() {
					super_mouse_exit();
					window_set_cursor(cr_default);
				}
	
				// Inherit left hold method
				super_mouse_left_hold = mouse_left_hold;
				method(self, super_mouse_left_hold);
				mouse_left_hold = function() {
					super_mouse_left_hold();				
					// Resize
					if (self.__resizing && self.resizable) {
						//ui_print("Resizing");
						width = max(min_width, width + (obj_UI.gui_mouse_x - obj_UI.gui_mouse_x_prev));
						height = max(min_height, height + (obj_UI.gui_mouse_y - obj_UI.gui_mouse_y_prev));	
					}
					// Drag
					else if (self.__dragging && self.draggable) {					
						//ui_print("Dragging");
						x += (obj_UI.gui_mouse_x - obj_UI.gui_mouse_x_prev);
						y += (obj_UI.gui_mouse_y - obj_UI.gui_mouse_y_prev);	
					}
				}
			
				// Inherit left release method
				super_mouse_left_release = mouse_left_release;
				method(self, super_mouse_left_hold);
				mouse_left_release = function() {
					super_mouse_left_release();	
					
					self.__dragging = false;
					self.__resizing = false;
					self.__drag_resize_point_x = -1;
					self.__drag_resize_point_y = -1;
					window_set_cursor(cr_default);
				}
			#endregion
			
			destroy = function() {
				// Destroy grids
				var _n = array_length(ui_grids);
				for (var _i=0; _i<_n; _i++) {
					ui_grids[_i].destroy();
				}
				// Mark panel for deletion
				array_push(obj_UI.mark_for_deletion, self);
				if (obj_UI.debug) ui_print("Destroyed");
			}
			
			
			// Add to UI panel array
			array_push(ui_panels, self);
			// Add to global UI map
			ui_add_to_map(_name, type, self);
			// Allow chained commands
			return self;
		}

	#endregion

	#region Grid

		function Grid(_name, _num_rows, _num_cols, _sizing = SIZING.RELATIVE) : Widget(_name, 0, 0, 1, 1, _sizing) constructor {
			type = UI_TYPE.GRID;
			// =========================
			// Styling
			// =========================
			border_width = 1;
			border_color = -1;
			back_color = -1;
			sizing = _sizing;
			panel_surfaces = [];
			vertical_scroll_offset = [];
			horizontal_scroll_offset = [];
			row_mouseover = -1;
			col_mouseover = -1;
			scrollable = false;
			
			parent_widget_id = noone;
			
			#region Data structures
			
				// Set rows and cols
				if (is_array(_num_rows)) {
					num_rows = array_length(_num_rows);
					ui_array_normalize(_num_rows);
					row_height = _num_rows;
				}
				else {
					num_rows = _num_rows;
					row_height = array_create(_num_rows, 1/_num_rows);
				}
			
				if (is_array(_num_cols)) {
					num_cols = array_length(_num_cols);
					ui_array_normalize(_num_cols);
					col_width = _num_cols;
				}
				else {
					num_cols = _num_cols;
					col_width = array_create(_num_cols, 1/_num_cols);
				}
			
				padding = 1;

				// Generate grid data structure and arrays to store children (each cell of the grid has an empty array to store children)
				grid_cells = array_create(num_rows);				
				panel_surfaces = array_create(num_rows);
				panel_surfaces_dimensions = array_create(num_rows);
				panel_surfaces_positioning = array_create(num_rows);
				vertical_scroll_offset = array_create(num_rows);
				horizontal_scroll_offset = array_create(num_rows);
			
				for (var _i=0; _i<num_rows; _i++) {
					grid_cells[_i] = array_create(num_cols);
					panel_surfaces[_i] = array_create(num_cols, noone);
					panel_surfaces_dimensions[_i] = array_create(num_cols, 1);
					panel_surfaces_positioning[_i] = array_create(num_cols, POSITIONING.EXACT);
					vertical_scroll_offset[_i] = array_create(num_cols, 0);
					horizontal_scroll_offset[_i] = array_create(num_cols, 0);
				
					for (var _j=0; _j<num_cols; _j++) {
						grid_cells[_i][_j] = [];
					}
				}
				
				grid_cells_mouseover_row = -1;
				grid_cells_mouseover_col = -1;
			
			
			#endregion
			
			// Anchor to parent - note that padding is not considered here but rather inside the draw() method
			anchorToParent = function() {				
				if (parent_widget_id != noone) {
					if (instanceof(parent_widget_id) == "Panel") {
						x = parent_widget_id.x+padding;
						y = parent_widget_id.y+padding;
						width = parent_widget_id.width - 2 * padding;
						height = parent_widget_id.height - 2 * padding;
					}
					else if (instanceof(parent_widget_id) == "Grid") {						
						x = parent_widget_id.col_to_x(parent_grid_col)+padding;
						y = parent_widget_id.row_to_y(parent_grid_row)+padding;
												
						parent_grid_cell_width = parent_widget_id.total_col_width(parent_grid_col);
						parent_grid_cell_height = parent_widget_id.total_row_height(parent_grid_row);
						
						width = parent_grid_cell_width - 2 * padding;
						height = parent_grid_cell_height - 2 * padding;
					}
					
				}
			}
						
			// =========================
			// Methods
			// =========================
			
			#region Helpers
			
				col_to_x = function(_col) {
					if (parent_widget_id.type == UI_TYPE.PANEL) {
						var _curr_x = x;
						var _total_w = parent_widget_id.width - (num_cols+1) * padding;
						for (var _j=0; _j<_col; _j++) {
							_curr_x += col_width[_j] * _total_w + padding;
						}
						return _curr_x;
					}
					else if (type == UI_TYPE.GRID) {
						var _curr_x = x;
						var _total_w = parent_grid_cell_width - (num_cols+1) * padding;
						for (var _j=0; _j<_col; _j++) {
							_curr_x += col_width[_j] * _total_w + padding;
						}
						return _curr_x;
					}
				}
			
				row_to_y = function(_row) {
					if (parent_widget_id.type == UI_TYPE.PANEL) {
						var _curr_y = y;
						var _total_h = parent_widget_id.height - (num_rows+1) * padding;
						for (var _i=0; _i<_row; _i++) {
							_curr_y += row_height[_i] * _total_h + padding;
						}
						return _curr_y;
					}
					else if (type == UI_TYPE.GRID) {
						var _curr_y = y;
						var _total_h = parent_grid_cell_height - (num_rows+1) * padding;
						for (var _i=0; _i<_row; _i++) {
							_curr_y += row_height[_i] * _total_h + padding;
						}
						return _curr_y;
					}
				}
			
				total_col_width = function(_col) {
					if (parent_widget_id.type == UI_TYPE.PANEL) {
						var _total_w = parent_widget_id.width - (num_cols+1) * padding;					
					}
					else if (parent_widget_id.type == UI_TYPE.GRID) {
						var _total_w = parent_grid_cell_width - (num_cols+1) * padding;
					}	
					return _total_w * col_width[_col];
				}
			
				total_row_height = function(_row) {
					if (parent_widget_id.type == UI_TYPE.PANEL) {
						var _total_h = parent_widget_id.height - (num_rows+1) * padding;					
					}
					else if (parent_widget_id.type == UI_TYPE.GRID) {					
						var _total_h = parent_grid_cell_height - (num_rows+1) * padding;
					}	
					return _total_h * row_height[_row];
				}
			
				addGrid = function(_idx_grid, _grid_row, _grid_col, _default_cell_positioning=POSITIONING.EXACT) {
					_idx_grid.parent_widget_id = self;
					_idx_grid.parent_grid_row = _grid_row;
					_idx_grid.parent_grid_col = _grid_col;
					_idx_grid.x = col_to_x(_grid_col);
					_idx_grid.y = row_to_y(_grid_row);
					_idx_grid.parent_grid_cell_width = total_col_width(_grid_col);
					_idx_grid.parent_grid_cell_height = total_row_height(_grid_row);
					for (var _row=0; _row<_grid_row; _row++) {
						for (var _col=0; _col<_grid_col; _col++) {
							_idx_grid.panel_surfaces_positioning[_row][_col] = _default_cell_positioning;
						}
					}
					grid_cells[_grid_row][_grid_col] = _idx_grid;
					return _idx_grid;
				}
			
				addWidget = function(_idx_or_array, _grid_row, _grid_col) {
					if (is_array(_idx_or_array)) {
						var _n = array_length(_idx_or_array);
						for (var _i=0; _i<_n; _i++) {
							array_push(grid_cells[_grid_row][_grid_col], _idx_or_array[_i]);
							_idx_or_array[_i].parent_widget_id = self;
						}
					}
					else {
						array_push(grid_cells[_grid_row][_grid_col], _idx_or_array);
						_idx_or_array.parent_widget_id = self;
					}
					return _idx_or_array;
				}
				
			#endregion
			
			__super_update = __update;
			method(self, __super_update);
			__update = function() {
				
				__super_update();
												
				// Determine mouseover cell
				grid_cells_mouseover_row = -1;
				grid_cells_mouseover_col = -1;
				
				var _row=0;
				var _col=0;
				var _found = false;
				while (_row<num_rows && _col<num_cols && !_found) {					
					if (point_in_rectangle(obj_UI.gui_mouse_x, obj_UI.gui_mouse_y, col_to_x(_col), row_to_y(_row), col_to_x(_col)+total_col_width(_col), row_to_y(_row)+total_row_height(_row))) {
						grid_cells_mouseover_row = _row;
						grid_cells_mouseover_col = _col;
						_found = true;
					}
					else {
						_col++;
						if (_col == num_cols) {
							_col = 0;
							_row++;
						}
					}
				}
				
				// Recursively launch child grid updates
				
				for (_row = 0; _row<num_rows; _row++) {
					for (_col = 0; _col<num_cols; _col++) {
						if (is_array(grid_cells[_row][_col])) { // Recursively update child widgets
							var _n = array_length(grid_cells[_row][_col]);
							for (var _i=0; _i<_n; _i++) {
								grid_cells[_row][_col][_i].__update();
							}
						}
						else { // Child grid
							grid_cells[_row][_col].__update();
						}
					}
				}
				
			}
			
			#region Draw
			
				draw = function() {
					anchorToParent();
					
					if (self.visible) {
					
						if (obj_UI.debug) draw_rectangle_width_color(x, y, x+width, y+height, 10, c_yellow, true);
				
						row_mouseover = -1;
						col_mouseover = -1;
				
						if (parent_widget_id != noone) {
												
							for (var _row=0; _row<num_rows; _row++) {
								for (var _col=0; _col<num_cols; _col++) {
								
									var _x1 = col_to_x(_col);
									var _y1 = row_to_y(_row);
									var _x2 = _x1 + total_col_width(_col);
									var _y2 = _y1 + total_row_height(_row);
								
								
									if (back_color != -1) draw_rectangle_color(_x1, _y1, _x2, _y2, back_color, back_color, back_color, back_color, false);
									if (border_color != -1) draw_rectangle_color(_x1, _y1, _x2, _y2, border_color, border_color, border_color, border_color, true);									
								
									// Set mouseover per surface
									if (point_in_rectangle(obj_UI.gui_mouse_x, obj_UI.gui_mouse_y, _x1, _y1, _x2, _y2)) {
										row_mouseover = _row;
										col_mouseover = _col;
									}
								
									if (is_array(grid_cells[_row][_col])) { // If this cell contains an array of widgets, it means we need to draw them to a surface, so create/resize it
									
										/// @TODO Remove surfaces if grid is not scrollable
									
									
										// Create or resize surface to hold the grid cell
										var _surf_w = max(total_col_width(_col), 1);
										var _surf_h = max(total_row_height(_row), 1);
										if (!surface_exists(panel_surfaces[_row][_col])) {
											panel_surfaces[_row][_col] = surface_create(_surf_w, _surf_h);
											//ui_print("created "+string(_surf_w)+"x"+string(_surf_h)+" surface at "+string(_row)+","+string(_col));
										}
										else if (abs(_surf_w - panel_surfaces_dimensions[_row][_col][0]) > 1 || abs(_surf_h - panel_surfaces_dimensions[_row][_col][1]) > 1) {
											surface_resize(panel_surfaces[_row][_col], _surf_w, _surf_h);
										}
														
										// Draw all children widgets to the surface, taking offsets and positioning for this cell into account
										var _num_widgets = array_length(grid_cells[_row][_col]);
										surface_set_target(panel_surfaces[_row][_col]);							
										draw_clear_alpha(c_black, obj_UI.debug ? 0.5 : 0);

										var _h = padding; // for automatic positioning only
										
										for (var _widget=0; _widget<_num_widgets; _widget++) {											
											// Draw according to positioning attribute
											switch (panel_surfaces_positioning[_row][_col]) {
												case POSITIONING.EXACT:
													var _widget_x = grid_cells[_row][_col][_widget].x + horizontal_scroll_offset[_row][_col];
													var _widget_y = grid_cells[_row][_col][_widget].y + vertical_scroll_offset[_row][_col];
													break;
												case POSITIONING.AUTOMATIC:
													var _widget_x = padding + horizontal_scroll_offset[_row][_col];
													var _widget_y = _h + vertical_scroll_offset[_row][_col];
													break;
												case POSITIONING.TOP_LEFT:
													var _widget_x = padding;
													var _widget_y = padding;
													break;
												case POSITIONING.TOP_CENTER:
													var _widget_x = _surf_w/2;
													var _widget_y = padding;
													break;
												case POSITIONING.TOP_RIGHT:
													var _widget_x = _surf_w-padding;
													var _widget_y = padding;
													break;
												case POSITIONING.MIDDLE_LEFT:
													var _widget_x = padding;
													var _widget_y = _surf_h/2;
													break;
												case POSITIONING.MIDDLE_CENTER:
													var _widget_x = _surf_w/2;
													var _widget_y = _surf_h/2;
													break;
												case POSITIONING.MIDDLE_RIGHT:
													var _widget_x = _surf_w-padding;
													var _widget_y = _surf_h/2;
													break;
												case POSITIONING.BOTTOM_LEFT:
													var _widget_x = padding;
													var _widget_y = _surf_h-padding;
													break;
												case POSITIONING.BOTTOM_CENTER:
													var _widget_x = _surf_w/2;
													var _widget_y = _surf_h-padding;
													break;
												case POSITIONING.BOTTOM_RIGHT:
													var _widget_x = _surf_w-padding;
													var _widget_y = _surf_h-padding;
													break;												
											}
											_h += grid_cells[_row][_col][_widget].draw(_widget_x, _widget_y, _x1, _y1) + padding;
										}
									
										surface_reset_target();
							
										// Draw the surface
										draw_surface(panel_surfaces[_row][_col], _x1, _y1);
										
										// Store w/h into variable
										panel_surfaces_dimensions[_row][_col] = [_surf_w, _surf_h];
									
									}
									else { // It's a new grid - draw it
										grid_cells[_row][_col].draw();
									}
							
								}
						
							}
						}
					}						
		
				}
			
			#endregion
			
			#region Events
			
				// Inherit mouse wheel increment method
				super_mouse_wheel_increment = mouse_wheel_increment;
				method(self, super_mouse_wheel_increment);
				mouse_wheel_increment = function() {
					super_mouse_wheel_increment();
					if (row_mouseover != -1 && col_mouseover != -1) {
						if (scrollable) {
							vertical_scroll_offset[row_mouseover][col_mouseover] += VERTICAL_SCROLL_SPEED;
						}
						else {
							vertical_scroll_offset[row_mouseover][col_mouseover] = 0;
						}
						if (obj_UI.debug) ui_print(ui_array_to_string(vertical_scroll_offset));
					}				
				}
				// Inherit mouse wheel decrement method
				super_mouse_wheel_decrement = mouse_wheel_decrement;
				method(self, super_mouse_wheel_decrement);
				mouse_wheel_decrement = function() {
					super_mouse_wheel_decrement();
					if (row_mouseover != -1 && col_mouseover != -1) {
						if (scrollable) {
							vertical_scroll_offset[row_mouseover][col_mouseover] -= VERTICAL_SCROLL_SPEED;
						}
						else {
							vertical_scroll_offset[row_mouseover][col_mouseover] = 0;
						}
						if (obj_UI.debug) ui_print(ui_array_to_string(vertical_scroll_offset));
					}
				
				}
			
			#endregion
			
			destroy = function() {
				for (var _row=0; _row<num_rows; _row++) {
					for (var _col=0; _col<num_cols; _col++) {
						if (surface_exists(panel_surfaces[_row][_col])) {
							surface_free(panel_surfaces[_row][_col]);
						}
						if (is_array(grid_cells[_row][_col])) {
							var _num_widgets = array_length(grid_cells[_row][_col]);
							for (var _widget=0; _widget<_num_widgets; _widget++) {
								grid_cells[_row][_col][_widget].destroy();
							}
						}
						else {
							grid_cells[_row][_col].destroy();
						}
					}
				}
				if (obj_UI.debug) ui_print("Destroyed");
			}
			
			
			// Add to global UI map
			ui_add_to_map(_name, type, self);
			// Allow chained commands
			return self;
		}
		
	#endregion

	#region Label
	
		function Label(_name, _x, _y, _text, _sizing=SIZING.ABSOLUTE) : Widget(_name, _x, _y, -1, -1, _sizing) constructor {
			type = UI_TYPE.LABEL;
			text = _text;
			
			// Default styling for non-Scribble
			text_color = c_white;
			text_font = -1;
			text_halign = fa_left;
			text_valign = fa_top;
			text_scale = 1;
			// Wrap settings for both Scribble and non-Scribble			
			text_wrap_width = -1;
			
			parent_widget_id = noone;
			
			draw_x = -1;
			draw_y = -1;
			
			
			// =========================
			// Methods
			// =========================
			
			super_update = __update;
			method(self, super_update);
			
			__update = function() {
				super_update();				
			}
			
			#region Draw
				draw = function(_x, _y, _surface_x, _surface_y) {	 // Note that child widgets are drawn relative to the surface!!!					
					if (self.visible) {						
						if (parent_widget_id != noone) {
							if (variable_global_exists("__scribble_lcg")) { // Scribble is available							
								var _t = scribble(text);
								if (text_wrap_width != -1) {
									_t.wrap(text_wrap_width);
								}
								_t.draw(_x, _y);
								width = _t.get_width();
								height = _t.get_height();
								var _bbox = _t.get_bbox(_x, _y);
								draw_x = _surface_x+_bbox.left;
								draw_y = _surface_y+_bbox.top;
							}
							else {					
								draw_set_color(text_color);
								draw_set_font(text_font);
								draw_set_halign(text_halign);
								draw_set_valign(text_valign);
								width = string_width(text);
								height = string_height(text);
								if (text_wrap_width != -1) {
									draw_text_ext_transformed(_x, _y, text, -1, text_wrap_width, text_scale, text_scale, 0);
								}
								else {
									draw_text_transformed(_x, _y, text, text_scale, text_scale, 0);
								}
								var _bbox = text_get_bbox(_x, _y, width, height);
								draw_x = _surface_x+_x;
								draw_y = _surface_y+_y;
							}
							
							if (obj_UI.debug) {
								draw_set_alpha(0.3);
								draw_rectangle_color(_bbox.left, _bbox.top, _bbox.right, _bbox.bottom, c_blue, c_blue, c_blue, c_blue, false);
								draw_set_alpha(1);
							}
						}
						return height;
					}
					else {
						return 0;
					}
				}
			#endregion
			
			destroy = function() {
				if (obj_UI.debug) ui_print("Destroyed");
			}
		
			// Add to global UI map
			ui_add_to_map(_name, type, self);
			// Allow chained commands
			return self;
		}
		
	#endregion

#endregion

#region User-facing functions

	function ui_get_panel_id(_name) {
		var _n = array_length(ui_panels);
		var _i = 0;
		while (_i<_n && ui_panels[_i].name != _name) {
			_i++;
		}
		if (_i<_n) {
			return ui_panels[_i];
		}
		else {
			return -1;
		}
	}
		
	function ui_get_parent_panel_id(_id) {
		var _panel_id;
		// Get parent Panel ID
		if (_id.type == UI_TYPE.PANEL) {
			_panel_id = _id;
		}
		else {
			_panel_id = parent_widget_id;
			while (_panel_id.type != UI_TYPE.PANEL) {
				_panel_id = _panel_id.parent_widget_id;
			}			
		}
		
		return _panel_id;
	}
	
	function ui_exists(_name) {
		return ds_map_exists(ui, _name);
	}	
	
#endregion
